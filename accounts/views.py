from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from accounts.forms import AccountForm, SignupForm
from django.contrib.auth.models import User


# Create your views here.
def log_in(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            user = authenticate(
                username=form.cleaned_data["username"],
                password=form.cleaned_data["password"],
            )
            if user:
                login(request, user)
                return redirect("home")
    else:
        form = AccountForm()

    context = {"form": form}

    return render(request, "accounts/login.html", context)


def log_out(request):
    logout(request)
    return redirect("login")


def sign_up(request):
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password == password_confirmation:
                # create a new user with those values and save
                # it as a variable
                user = User.objects.create_user(username, password=password)
                user.save()
                login(request, user)
                return redirect("home")
    else:
        form = SignupForm()

    context = {"form": form}

    return render(request, "accounts/signup.html", context)
